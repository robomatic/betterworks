import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Welcome from "../views/Welcome.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/welcome",
    name: "Welcome",
    component: Welcome,
  },
  {
    path: "/listing",
    name: "Listing",
    component: () =>
      import(/* webpackChunkName: "listing" */ "../views/Listing.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
