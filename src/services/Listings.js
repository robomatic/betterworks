const data = {
  1: { name: "Number Uno", subtitle: "The first one!" },
  10: { name: "Number Ten", subtitle: "The tenth one!" },
  2: { name: "Johnny Magic", subtitle: "Has a rad car." },
  33: { name: "OK", subtitle: "What?" },
  4: { name: "Hello World", subtitle: "Hi tiny world." },
  555: { name: "Cinco Cinco Cinco", subtitle: "Sunday Sunday Sunday" },
  6: {
    name: "Person Six",
    subtitle:
      "There is not enough data in the universe to describe this particular entry",
  },
  7: { name: "George M.", subtitle: "" },
  8: { name: "Michael J. Fox", subtitle: null },
  9: { name: "Captain C.", subtitle: "Super Cool" },
  "000": { name: "Edwin C.", subtitle: "___.___.___" },
  a: { name: "Josh I.", subtitle: "The coolest." },
  b: { name: "Todd_C", subtitle: "Why?" },
  aa: { name: "Ol' Hambone", subtitle: 13 },
  cde: { name: "Beef Supreme", subtitle: [] },
  f: { name: "Tim_ERIC", subtitle: "A & B, 123" },
  ghi: { name: "", subtitle: "Who is this?" },
  k: { name: "someone Else", subtitle: { abc: "ok" } },
  hello: { name: "Okee Dokey", subtitle: "Artichokey" },
  "there!": { name: "Half-Shark-Alligator-Half-Man", subtitle: "Kool" },
  " ": { name: "", subtitle: "098" },
  nope: { name: null, subtitle: "" },
  awesome: {
    name: "The Champ",
    subtitle:
      "ZZZBCBJJBJBEBFJBEWBFBEWWJBJBFEWFBEWJBFJJEBWJFEW#R#@J#BJQBBQWDWQWDBJBJBDQWBJBJEWFBF",
  },
};

export default function Listings(filter, limit = 10) {
  let result, count;
  // convert to array
  result = Object.keys(data).map((d) => ({
    id: d,
    ...data[d],
  }));
  // set the count before limit for un-filtered results
  count = result.length;
  if (filter) {
    const regex = new RegExp(filter, "gi");
    result = result
      .filter((item) => item.name && regex.test(item.name))
      .slice(0, limit);
    count = result.length;
  } else {
    result = result.slice(0, limit);
  }
  return { count, data: result };
}
